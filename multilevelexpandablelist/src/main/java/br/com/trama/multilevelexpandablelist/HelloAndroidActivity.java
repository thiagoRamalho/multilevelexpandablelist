package br.com.trama.multilevelexpandablelist;

import java.util.ArrayList;
import java.util.List;

import br.com.trama.multilevelexpandablelist.simple.FirsLevelAdapter;
import br.com.trama.multilevelexpandablelist.simple.First;
import br.com.trama.multilevelexpandablelist.simple.Second;
import br.com.trama.multilevelexpandablelist.simple.Third;
import android.app.Activity;
import android.os.Bundle;
import android.widget.ExpandableListView;

public class HelloAndroidActivity extends Activity {

	private ExpandableListView expListView;
	
	/**
	 * Called when the activity is first created.
	 * @param savedInstanceState If the activity is being re-initialized after 
	 * previously being shut down then this Bundle contains the data it most 
	 * recently supplied in onSaveInstanceState(Bundle). <b>Note: Otherwise it is null.</b>
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		expListView = (ExpandableListView) findViewById(R.id.ParentLevel);
		
		expListView.setAdapter(new FirsLevelAdapter(create()));
		
		expListView.setGroupIndicator(null);
	}
	
	private List<First> create(){
	
		List<First> first = new ArrayList<First>();
		
		for (int i = 0; i < 3; i++) {
			
			First f = new First("Root ["+i+"]");
			first.add(f);
			
			for (int j = 0; j < 3; j++) {
				
				Second s = new Second("Second ["+j+"]");
				f.getSeconds().add(s);
				
				for (int k = 0; k < 3; k++) {
					
					Third t = new Third("Third ["+k+"]");
					s.getThirds().add(t);
				}
			}
		}
		
		return first;
	}
}

