package br.com.trama.multilevelexpandablelist.simple;

import java.util.List;

import br.com.trama.multilevelexpandablelist.CustExpListview;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

public class FirsLevelAdapter extends BaseExpandableListAdapter {

	private List<First> firsts;
	
	public FirsLevelAdapter(List<First> firsts) {
		super();
		this.firsts = firsts;
	}

	public int getGroupCount() {
		return firsts.size();
	}

	public int getChildrenCount(int groupPosition) {
		return firsts.get(groupPosition).getSeconds().size();
	}

	public First getGroup(int groupPosition) {
		return firsts.get(groupPosition);
	}

	public Second getChild(int groupPosition, int childPosition) {
		return getGroup(groupPosition).getSeconds().get(childPosition);
	}

	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	public boolean hasStableIds() {
		return false;
	}

	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		
		Log.d(this.getClass().getSimpleName(), "getGroupView");

		
		TextView textView = new TextView(parent.getContext());
		textView.setText(getGroup(groupPosition).getName());
		
		return textView;
	}

	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		
		Log.d(this.getClass().getSimpleName(), "getChildView");
		
		SecLevelAdapter secLevelAdapter = new SecLevelAdapter(getChild(groupPosition, childPosition));
		
		ExpandableListView expandableListView = new CustExpListview(parent.getContext());
		expandableListView.setAdapter(secLevelAdapter);
		
		expandableListView.setGroupIndicator(null);
		
		return expandableListView;
	}

	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}
}
