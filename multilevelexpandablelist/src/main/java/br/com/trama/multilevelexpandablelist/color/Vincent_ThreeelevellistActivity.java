package br.com.trama.multilevelexpandablelist.color;

import br.com.trama.multilevelexpandablelist.R;
import br.com.trama.multilevelexpandablelist.R.id;
import br.com.trama.multilevelexpandablelist.R.layout;
import android.app.Activity;
import android.os.Bundle;
import android.widget.ExpandableListView;
 

public class Vincent_ThreeelevellistActivity extends Activity {
      
	ExpandableListView explvlist;
 
       
       public void onCreate(Bundle savedInstanceState) {
              super.onCreate(savedInstanceState);
              setContentView(R.layout.activity_main);
 
              explvlist = (ExpandableListView) findViewById(R.id.ParentLevel);
              explvlist.setAdapter(new ParentLevel());
 
       }
}

