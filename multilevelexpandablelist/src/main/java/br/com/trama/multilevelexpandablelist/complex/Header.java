package br.com.trama.multilevelexpandablelist.complex;

import java.util.ArrayList;
import java.util.List;

public class Header {

	private String name;
	private String number;
	private String other;
	
	private List<String> team;
	
	public Header(String name, String number, String other) {
		super();
		this.name = name;
		this.number = number;
		this.other = other;
		this.team = new ArrayList<String>();
	}

	public void addMemberTeam(String name){
		this.team.add(name);
	}
	
	public String getName() {
		return name;
	}

	public String getNumber() {
		return number;
	}

	public String getOther() {
		return other;
	}

	public String getMemberAt(int childPosition) {
		return this.team.get(childPosition);
	}

	public int teamSize() {
		return this.team.size();
	}
	
	public List<String> getTeam() {
		return team;
	}
}
