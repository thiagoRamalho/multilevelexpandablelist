package br.com.trama.multilevelexpandablelist.simple;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

public class SecLevelAdapter extends BaseExpandableListAdapter{

	private Second seconds;
	
	public SecLevelAdapter(Second second) {
		super();
		this.seconds = second;
	}

	public int getGroupCount() {
		return 1;
	}

	public int getChildrenCount(int groupPosition) {
		return this.seconds.getThirds().size();
	}

	public Second getGroup(int groupPosition) {
		return this.seconds;
	}

	public Third getChild(int groupPosition, int childPosition) {
		return getGroup(groupPosition).getThirds().get(childPosition);
	}

	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	public boolean hasStableIds() {
		return true;
	}

	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		
		Log.d(this.getClass().getSimpleName(), "getGroupView");
		
		TextView textView = new TextView(parent.getContext());
		textView.setText(getGroup(groupPosition).getName());
		
		return textView;
	}

	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		
		Log.d(this.getClass().getSimpleName(), "getChildView");
		
		Third child = getChild(groupPosition, childPosition);
		
		TextView textView = new TextView(parent.getContext());
		textView.setText(child.getName());
		
		return textView;
	}

	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
}
