package br.com.trama.multilevelexpandablelist.simple;

import java.util.ArrayList;
import java.util.List;

public class Second {

	private String name;
	
	private List<Third> thirds = new ArrayList<Third>();

	public Second(String name) {
		super();
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public List<Third> getThirds() {
		return thirds;
	}
}
