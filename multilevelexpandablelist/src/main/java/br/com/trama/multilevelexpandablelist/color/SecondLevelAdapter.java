package br.com.trama.multilevelexpandablelist.color;

import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseExpandableListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class SecondLevelAdapter extends BaseExpandableListAdapter {

	public Object getChild(int groupPosition, int childPosition) {
		return childPosition;
	}


	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}


	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		TextView tv = new TextView(parent.getContext());
		tv.setText("child");
		tv.setTextColor(Color.BLACK);
		tv.setTextSize(20);
		tv.setPadding(15, 5, 5, 5);
		tv.setBackgroundColor(Color.YELLOW);
		tv.setLayoutParams(new ListView.LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		return tv;
	}


	public int getChildrenCount(int groupPosition) {
		return 5;
	}


	public Object getGroup(int groupPosition) {
		return groupPosition;
	}


	public int getGroupCount() {
		return 1;
	}


	public long getGroupId(int groupPosition) {
		return groupPosition;
	}


	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		TextView tv = new TextView(parent.getContext());
		tv.setText("-->Second Level");
		tv.setTextColor(Color.BLACK);
		tv.setTextSize(20);
		tv.setPadding(12, 7, 7, 7);
		tv.setBackgroundColor(Color.RED);

		return tv;
	}

	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return true;
	}


	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return true;
	}

}