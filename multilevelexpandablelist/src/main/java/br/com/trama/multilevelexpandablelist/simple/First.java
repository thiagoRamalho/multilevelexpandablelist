package br.com.trama.multilevelexpandablelist.simple;

import java.util.ArrayList;
import java.util.List;

public class First {

	private String name;
	
	private List<Second> seconds = new ArrayList<Second>();

	public First(String name) {
		super();
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public List<Second> getSeconds() {
		return seconds;
	}
}
