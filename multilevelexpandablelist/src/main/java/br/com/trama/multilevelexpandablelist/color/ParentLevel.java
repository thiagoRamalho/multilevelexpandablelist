package br.com.trama.multilevelexpandablelist.color;

import br.com.trama.multilevelexpandablelist.CustExpListview;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
   public class ParentLevel extends BaseExpandableListAdapter {

          public Object getChild(int arg0, int arg1) {
                 return arg1;
          }

          public long getChildId(int groupPosition, int childPosition) {
                 return childPosition;
          }

          public View getChildView(int groupPosition, int childPosition,
                       boolean isLastChild, View convertView, ViewGroup parent) {
        	  
                 CustExpListview SecondLevelexplv = new CustExpListview(parent.getContext());
                 SecondLevelexplv.setAdapter(new SecondLevelAdapter());
                 SecondLevelexplv.setGroupIndicator(null);
              
                 return SecondLevelexplv;
          }

          public int getChildrenCount(int groupPosition) {
                 return 3;
          }

          public Object getGroup(int groupPosition) {
                 return groupPosition;
          }

          public int getGroupCount() {
                 return 5;
          }

          public long getGroupId(int groupPosition) {
                 return groupPosition;
          }

          public View getGroupView(int groupPosition, boolean isExpanded,
                       View convertView, ViewGroup parent) {
                 TextView tv = new TextView(parent.getContext());
                 tv.setText("->FirstLevel");
                 tv.setTextColor(Color.BLACK);
                 tv.setTextSize(20);
                 tv.setBackgroundColor(Color.BLUE);
                 tv.setPadding(10, 7, 7, 7);

                 return tv;
          }

          
          public boolean hasStableIds() {
                 return true;
          }

          
          public boolean isChildSelectable(int groupPosition, int childPosition) {
                 return true;
          }
   }