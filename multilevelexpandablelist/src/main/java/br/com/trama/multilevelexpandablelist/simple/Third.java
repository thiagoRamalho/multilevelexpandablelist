package br.com.trama.multilevelexpandablelist.simple;

public class Third {

	private String name;

	public Third(String name) {
		super();
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}
