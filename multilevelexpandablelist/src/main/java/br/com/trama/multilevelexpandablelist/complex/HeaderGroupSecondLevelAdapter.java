package br.com.trama.multilevelexpandablelist.complex;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import br.com.trama.multilevelexpandablelist.R;

public class HeaderGroupSecondLevelAdapter extends BaseExpandableListAdapter{

	private Header header;
	private LayoutInflater inflater;

	public HeaderGroupSecondLevelAdapter(Header header, LayoutInflater inflater) {
		this.header = header;
		this.inflater = inflater;
	}

	public int getGroupCount() {
		return 1;
	}

	public int getChildrenCount(int groupPosition) {
		return 0;
	}

	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return null;
	}

	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return null;
	}

	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return 0;
	}

	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return 0;
	}

	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		
		Log.d(this.getClass().getSimpleName(), "getGroupView");
		
		if(convertView == null){
			convertView = inflater.inflate(R.layout.header2, parent, false);
		}
		
		TextView name = (TextView)convertView.findViewById(R.id.name);
		name.setText(this.header.getName());
		
		ListView listView = (ListView) convertView.findViewById(R.id.listView1);
		
		Log.d(this.getClass().getSimpleName(), "listView != null "+(listView != null));

		ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(parent.getContext(), android.R.layout.simple_list_item_1, this.header.getTeam());
		
		listView.setAdapter(arrayAdapter);
		
		return convertView;
	}

	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return false;
	}

}
