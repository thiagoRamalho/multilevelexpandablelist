package br.com.trama.multilevelexpandablelist;

import java.util.ArrayList;
import java.util.List;

import br.com.trama.multilevelexpandablelist.complex.ContainerAdapter;
import br.com.trama.multilevelexpandablelist.complex.Header;
import br.com.trama.multilevelexpandablelist.simple.FirsLevelAdapter;
import br.com.trama.multilevelexpandablelist.simple.First;
import br.com.trama.multilevelexpandablelist.simple.Second;
import br.com.trama.multilevelexpandablelist.simple.Third;
import android.app.Activity;
import android.os.Bundle;
import android.widget.ExpandableListView;

public class ComplexAndroidActivity extends Activity {

	private ExpandableListView expListView;
	
	/**
	 * Called when the activity is first created.
	 * @param savedInstanceState If the activity is being re-initialized after 
	 * previously being shut down then this Bundle contains the data it most 
	 * recently supplied in onSaveInstanceState(Bundle). <b>Note: Otherwise it is null.</b>
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.complex);
		
		expListView = (ExpandableListView) findViewById(R.id.ParentLevel);
		
		expListView.setAdapter(new ContainerAdapter(create(), this.getLayoutInflater()));
		
		expListView.setGroupIndicator(null);
	}
	
	private Header create(){
		
		Header header = new Header("name", "123", "other");
		
		for(int i = 0; i < 3; i++){
			header.addMemberTeam("member with name is very large ["+i+"]");
		}
		
		return header;
	}
}

