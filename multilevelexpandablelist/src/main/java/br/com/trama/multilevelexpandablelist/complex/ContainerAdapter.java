package br.com.trama.multilevelexpandablelist.complex;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;
import br.com.trama.multilevelexpandablelist.CustExpListview;

public class ContainerAdapter extends BaseExpandableListAdapter{

	private Header header;
	private LayoutInflater inflater;
	
	public ContainerAdapter(Header header, LayoutInflater inflater) {
		super();
		this.header = header;
		this.inflater = inflater;
	}

	public int getGroupCount() {
		return 1;
	}

	public int getChildrenCount(int groupPosition) {
		return 1;
	}

	public Header getGroup(int groupPosition) {
		return header;
	}

	public Header getChild(int groupPosition, int childPosition) {
		return header;
	}

	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	public boolean hasStableIds() {
		return false;
	}

	public View getGroupView(int groupPosition, boolean isExpanded,
			
			View convertView, ViewGroup parent) {
		
		Log.d(this.getClass().getSimpleName(), "getGroupView");

		TextView textView = new TextView(parent.getContext());
		textView.setText("Filtros");
		
		return textView;
	}

	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		
		Log.d(this.getClass().getSimpleName(), "getChildView");
		
		HeaderGroupSecondLevelAdapter secLevelAdapter = new HeaderGroupSecondLevelAdapter(this.header, this.inflater);
		
		ExpandableListView expandableListView = new CustExpListview(parent.getContext());
		expandableListView.setAdapter(secLevelAdapter);
		
		expandableListView.setGroupIndicator(null);
		
		return expandableListView;
	}

	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}
}
